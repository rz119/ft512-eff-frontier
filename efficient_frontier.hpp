//
//  efficient_frontier.hpp
//  C++project
//
//  Created by 朱若楠 on 2021/11/19.
//

#ifndef efficient_frontier_hpp
#define efficient_frontier_hpp

#include <stdio.h>
#include <iostream>
#include <Eigen/Dense>
#include <vector>
using namespace std;
using Eigen::MatrixXd;

class Asset{
private:
    string name;
    double ret;
    double std;
public:
    Asset(){};
    ~Asset(){};
    Asset(string n, double r, double s):name(n),ret(r),std(s){};
    string get_name();
    double get_ret();
    double get_std();
    void set_name(string);
    void set_ret(double);
    void set_std(double);
    void print_asset();
};


class Portfolio{
private:
    int size;
    vector<Asset> assets;
    MatrixXd corr_matrix;
public:
    Portfolio():size(0){
      vector<Asset> assets;
      MatrixXd corr_matrix;
    };
    Portfolio(int s, vector<Asset> a, MatrixXd c):size(s),assets(a),corr_matrix(c){};
    ~Portfolio(){};
    int get_size();
    Asset get_asset(int);
    MatrixXd get_corr_matrix();
    MatrixXd get_cov_matrix();
    Eigen::VectorXd build_ret_vec();
    Eigen::VectorXd get_weight(double, MatrixXd, Eigen::VectorXd);
    Eigen::VectorXd get_weight_r(double, MatrixXd, Eigen::VectorXd, Eigen::VectorXd);
    double get_port_std(Eigen::VectorXd, MatrixXd);
    double get_port_ret(Eigen::VectorXd, Eigen::VectorXd);

    void add_asset(Asset&); // add 1 asset, size+1
    void add_corr_matrix(MatrixXd&);
};



#endif /* efficient_frontier_hpp */
