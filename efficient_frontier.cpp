//
//  efficient_frontier.cpp
//  C++project
//
//  Created by 朱若楠 on 2021/11/19.
//

#include "efficient_frontier.hpp"
#include <fstream>
#include <cmath>
#include <unistd.h>
using namespace std;

void Asset::set_name(string str){
    name = str;
    return;
}

void Asset::set_ret(double r){
    ret = r;
    return;
}

void Asset::set_std(double s){
    std = s;
    return;
}

void Asset::print_asset(){
    cout << "asset name: " << name << ", ret: " << ret << ", std: " << std << endl;
    return;
}

string Asset::get_name(){
    return name;
}

double Asset::get_ret(){
    return ret;
}

double Asset::get_std(){
    return std;
}

int Portfolio::get_size(){
    return size;
}

MatrixXd Portfolio::get_corr_matrix(){
    return corr_matrix;
}

Asset Portfolio::get_asset(int loc){
    if (loc >= size) {
        cerr << "asset overflow!" << endl;
        exit(EXIT_FAILURE);
    }
    return assets[loc];
}

void Portfolio::add_asset(Asset & asset){
    assets.push_back(asset);
    size++;
    return;
}


void Portfolio::add_corr_matrix(MatrixXd & corr){
    if (size != corr.rows() || size!=corr.cols() || corr.rows()!=corr.cols()){
        cerr << "Invalid correlation matrix!" << endl;
        exit(EXIT_FAILURE);
    }
    corr_matrix = corr;
    return;
}

MatrixXd Portfolio::get_cov_matrix(){
    MatrixXd cov(size, size);
    for (int i=0; i<size; i++){
        for (int j=0; j<size; j++){
            cov(i, j) = corr_matrix(i, j) * assets[i].get_std() * assets[j].get_std();
        }
    }
    return cov;
}


Eigen::VectorXd Portfolio::build_ret_vec(){
    Eigen::VectorXd ret_vec(size);
    for (int i=0; i<size; i++){
        ret_vec[i] = assets[i].get_ret();
    }
    return ret_vec;
}

MatrixXd build_A(int size, MatrixXd cov, Eigen::VectorXd ret_vec){
    MatrixXd A = MatrixXd::Zero(size+2, size+2);
    for (int i=0; i<size; i++){
        for (int j=0; j<size; j++){
            A(i, j) = 2 * cov(i, j);
        }
    }
    for (int j=0; j<size; j++){
        A(size, j) = ret_vec[j];
        A(size+1, j) = 1;
        A(j, size) = ret_vec[j];
        A(j, size+1) = 1;
    }
    return A;
}


Eigen::VectorXd build_b(int size, double exp_ret){
    Eigen::VectorXd b(size+2);
    for (int i=0; i<size; i++){
        b[i] = 0;
    }
    b[size] = exp_ret;
    b[size+1] = 1;
    return b;
}


Eigen::VectorXd Portfolio::get_weight(double exp_ret, MatrixXd cov, Eigen::VectorXd ret_vec){
    MatrixXd A = build_A(size, cov, ret_vec);
    Eigen::VectorXd b = build_b(size, exp_ret);
    return (A.inverse()*b).head(size);
}


int find_max_nega_weight(Eigen::VectorXd weight, int size){
    double max_weight = 0;
    int max_loc = -1;
    for (int i=0; i<size; i++){
        if (weight[i] < max_weight){
            max_weight = weight[i];
            max_loc = i;
        }
    }
    return max_loc;
}


Eigen::VectorXd Portfolio::get_weight_r(double exp_ret, MatrixXd cov,
                                        Eigen::VectorXd ret_vec,
                                        Eigen::VectorXd weight_u){
    MatrixXd A = build_A(size, cov, ret_vec);
    Eigen::VectorXd b = build_b(size, exp_ret);
    int max_loc = find_max_nega_weight(weight_u, size);
    while (max_loc != -1){
        MatrixXd A_adj = MatrixXd::Zero(A.rows()+1, A.cols()+1);
        A_adj.topLeftCorner(A.rows(), A.cols()) = A;
        A_adj(max_loc, A.cols()) = 1;
        A_adj(A.rows(), max_loc) = 1;
        Eigen::VectorXd b_adj = Eigen::VectorXd::Zero(A_adj.cols());
        b_adj.head(b.size()) = b;
        Eigen::VectorXd temp_weight = (A_adj.inverse()*b_adj).head(size);
        max_loc = find_max_nega_weight(temp_weight, size);
        if (max_loc == -1){
            return temp_weight;
        }
        A = MatrixXd::Zero(A_adj.rows(), A_adj.cols());
        A.topLeftCorner(A_adj.rows(), A_adj.cols()) = A_adj;
    }
    return weight_u;
}


//Eigen::VectorXd Portfolio::get_weight_r(double exp_ret, MatrixXd cov,
//                                        Eigen::VectorXd ret_vec,
//                                        Eigen::VectorXd weight_u){
//    vector<int> nega_nums = find_negative_weight(weight_u, size);
//    unsigned long nega_size = nega_nums.size();
//    while (nega_size > 0){
//        MatrixXd A = MatrixXd::Zero(size+2+nega_size, size+2+nega_size);
//        for (int i=0; i<size; i++){
//            for (int j=0; j<size; j++){
//                A(i, j) = 2 * cov(i, j);
//            }
//        }
//        for (int j=0; j<size; j++){
//            A(size, j) = ret_vec[j];
//            A(size+1, j) = 1;
//            A(j, size) = ret_vec[j];
//            A(j, size+1) = 1;
//        }
//        for (unsigned int i=0; i<nega_size; i++){
//            A(nega_nums[i], size+2+i) = 1;
//            A(size+2+i, nega_nums[i]) = 1;
//        }
//
//        Eigen::VectorXd b(size+2+nega_size);
//        for (int i=0; i<size+2+nega_size; i++){
//            b[i] = 0;
//        }
//        b[size] = exp_ret;
//        b[size+1] = 1;
//
//        Eigen::VectorXd temp_weight = (A.inverse()*b).head(size);
//        for (int i=0; i<size; i++){
//            if (temp_weight[i] < 0){
//                nega_nums.push_back(i);
//            }
//        }
//        if (nega_size == nega_nums.size()){
//            return temp_weight;
//        }
//        nega_size = nega_nums.size();
//    }
//    return weight_u;
//}

double Portfolio::get_port_std(Eigen::VectorXd weight, MatrixXd cov){
    double ans = 0;
    for (int i=0; i<size; i++){
        for (int j=0; j<size; j++){
            ans += weight[i] * weight[j] * cov(i, j);
        }
    }
    return sqrt(ans);
}

double Portfolio::get_port_ret(Eigen::VectorXd weight, Eigen::VectorXd ret_vec){
    double ans = 0;
    for (int i=0; i<size; i++){
        ans += weight[i] * ret_vec[i];
    }
    return ans;
}



size_t find_position(string str, size_t start, char symbol){
    if (start >= str.length()){
        cerr << "position exceeds string length!" << endl;
        exit(EXIT_FAILURE);
    }
    for (size_t i=start; i<str.length(); i++){
        if (str[i] == symbol)
            return i;
    }
    return -1;
}

int find_all_pos(string str, char symbol){
    int num = 0;
    for (size_t i=0; i<str.length(); i++){
        if (str[i] == symbol)
            num++;
    }
    return num;
}


double element_tod(string substr){
    try{
        double value = stod(substr);
        return value;
    }
    catch(exception& e){
        cerr << "Invalid element!" << endl;
        exit(EXIT_FAILURE);
    }
}


Asset create_asset(string line){
    Asset asset;
    size_t first = find_position(line, 0, ',');
    size_t second = find_position(line, first+1, ',');
    size_t third = find_position(line, second+1, ',');
    if (first == -1 || second == -1){
        cerr << "Absent return or sd!" << endl;
        exit(EXIT_FAILURE);
    }
    if (third != -1){
        cerr << "Invalid lines in asset_file!" << endl;
        exit(EXIT_FAILURE);
    }
    asset.set_name(line.substr(0, first));
    asset.set_ret(element_tod(line.substr(first+1, second-first-1)));
    asset.set_std(element_tod(line.substr(second+1, line.length()-second-1)));
    //    asset.print_asset();
    return asset;
}


MatrixXd create_corr_matrix(ifstream& file, int size){
    MatrixXd corr(size, size);
    string line;
    for (int i=0; i<size; i++){
        if (getline(file, line)){
            int all_pos = find_all_pos(line, ',');
            if (all_pos+1 != size){
                cerr << "assets and correlations unmatched!" << endl;
                exit(EXIT_FAILURE);
            }
            size_t start = 0;
            for (int j=0; j<all_pos; j++){
                size_t end = find_position(line, start, ',');
                corr(i, j) = element_tod(line.substr(start, end-start));
                start = end + 1;
            }
            corr(i, size-1) = element_tod(line.substr(start, line.length()-start));
        }
        if (file.eof()){
            cerr << "Less sizes for corr_file!" << endl;
            exit(EXIT_FAILURE);
        }
    }
    if (!file.eof()){
        cerr << "More sizes for corr_file!" << endl;
        exit(EXIT_FAILURE);
    }
    return corr;
}


void print_eff_frontier(Portfolio * port, double max_ret_percent,
            MatrixXd cov, Eigen::VectorXd ret_vec, char flag = 'u'){
    cout <<  "ROR,volatility" << endl;
    cout << fixed;
    for (double i=1; i<=max_ret_percent; i++){
        Eigen::VectorXd weight = port->get_weight(i/100, cov, ret_vec);
        if (flag == 'r'){
            weight = port->get_weight_r(i/100, cov, ret_vec, weight);
        }
        cout << setprecision(1) << port->get_port_ret(weight, ret_vec)*100 << "%,";
        cout << setprecision(2) << port->get_port_std(weight, cov)*100 << "%"<< endl;
    }
    return;
}

void check_validity(ifstream& file){
    if (!file.is_open()){
        cerr << "Cannot open files!" << endl;
        exit(EXIT_FAILURE);
    }
    if (file.eof()){
        cerr << "Empty files!" << endl;
        exit(EXIT_FAILURE);
    }
    return;
}

void check_lines(int size){
    if (size <= 1){
        cerr << "Invalid asset file with only "<< size << " line(s)!" << endl;
        exit(EXIT_FAILURE);
    }
    return;
}


void parse_file(char * argv1, char * argv2, char flag = 'u'){
    ifstream asset_file(argv1, ios::in);
    ifstream corr_file(argv2, ios::in);
    check_validity(asset_file);
    check_validity(corr_file);
    // build portfolio
    string line;
    Portfolio port;
    while (getline(asset_file, line)){
        Asset asset = create_asset(line);
        port.add_asset(asset);
    }
    int size = port.get_size();
    check_lines(size);
    MatrixXd corr = create_corr_matrix(corr_file, size);
    port.add_corr_matrix(corr);
    // calculate and print
    Eigen::VectorXd ret_vec = port.build_ret_vec();
    MatrixXd cov = port.get_cov_matrix();
    if (flag == 'u')
      print_eff_frontier(&port, 26, cov, ret_vec);
    else
      print_eff_frontier(&port, 26, cov, ret_vec, 'r');
    asset_file.close();
    corr_file.close();
}


int main(int argc, char * argv[]){
    char restricted = false;
    int o;
    const char* optstring = "r";
    while ((o = getopt(argc, argv, optstring)) != -1){
        switch(o){
            case 'r':
                restricted = true;
            case '?':
                cerr << "Invalid optional argument!" << endl;
                return EXIT_FAILURE;
        }
    }
    if (argc == 3){
        parse_file(argv[1], argv[2]);
    }
    else if (argc == 4){
        parse_file(argv[2], argv[3], 'r');
    }
    else {
        cerr << "Invalid command line argument!" << endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

